#!/usr/bin/env python
# Author: Pedro L. Fernandez / Luis M. Herrera
# Purpose: SNS publisher - publish to a SNS Topic data collected from Intruction Detection Sensor (IDS)
# Date: May 19, 2016
#
import logging
import json
import boto.sns
from idstools import unified2

reader = unified2.SpoolEventReader("/var/log/custome_snort","merged.log", follow=True)
for event in reader:
	eventDict = {}
	for item in event:
		if item not in ["source-ip.raw", "destination-ip.raw","packets", "appid"]:
			print "%s -> %s" % (item,event[item])  # instead of printing, store so you create a dict
 			eventDict[item] = event[item]		
	### Add code to create dict -> json from the event/item key/values store in the above function.
	# dict like   aDict = { "key1": "val1", "key2": "val2" } 
	# then convert to json using jsonMsg = json.dumps(aDict)
	# send json message using below code...
	
	newJsonEvent = json.dumps(eventDict)
	logging.basicConfig(filename="sns-publish.log", level=logging.DEBUG)

	c = boto.sns.connect_to_region("us-east-1")

	topicarn = ["arn:aws:sns:us-east-1:635184443559:attack","arn:aws:sns:us-east-1:726822058731:attack"]
	message_subject = "Cloud Enable Reporting"


	# transforming message to json format
	jsonMsg = newJsonEvent
	#publication = c.publish(topicarn[0], jsonMsg, subject=message_subject)
	publication = c.publish(topicarn[1], jsonMsg, subject=message_subject)

	print publication
