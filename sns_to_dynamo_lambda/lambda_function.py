from __future__ import print_function

import json
import boto.sns
import boto3

print('Loading function')


def lambda_handler(event, context):
    c = boto.sns.connect_to_region("us-east-1")
    topicarn = "arn:aws:sns:us-east-1:635184443559:lambdaResponse"

    # Unpacking sns json to snort json , Note: add some error cheking later...
    snortMsg = event["Records"][0]["Sns"]["Message"]
    snortDict = eval(snortMsg)

    # Dynamo stuff
    dynamodb = boto3.resource('dynamodb','us-east-1')
    dynamoTable = dynamodb.Table('snortsensor')

    # unpack values from dict
    protocol = snortDict["protocol"]
    destinationIp = snortDict["destination-ip"]
    sourceIp = snortDict["source-ip"]
    eventSecond = snortDict["event-second"]
    signatureId = snortDict["signature-id"]
     
    msg = { "seconds": eventSecond, "signatureid": signatureId, "soureceip": sourceIp, "destinationip": destinationIp, "protocol": protocol }
    dynamoTable.put_item(Item = msg)
