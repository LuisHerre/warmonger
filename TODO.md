## TODO List - ordered by priority

- [ ] add date & time to snort output (merged.log)
- [ ] add date & time to sns json message. (dependend on step 1) code: sensor_to_sns_parser.py
- [ ] add function to detect private vs. public address on source IP and destination IP. (using Python  IPy module) code: sensor_to_sns_parser.py
- [ ] add a geolocation function (https://pypi.python.org/pypi/geolocation-python/0.2.0 module)  (only if public address, otherwise say Internal IP -req step 3) - code: sensor_to_sns_parser.py
		geolocation should include the following fields
			* Country
			* Region
			* Lat/Lon
			* zipcode
- [ ] use the geolocation function and add geolocation data to to sns json message. (req on step 4) code: sensor_to_sns_parser.py
- [ ] enable more rules on snort to detect other attacks. (no dependency)
- [ ] Investigate how to add name/type of attack to merged.log on snort. (no dependency)
- [ ] ACTION: create trigger for DynamoDB to sync with Elastic Search on each Insert/Update/Delete. (no dependency)
- [ ] add new Alexa intent for action "how many attacks today"
- [ ] add new Alexa intent for action "top countries attacking our network"
- [ ] Add function to alexa_warmonger_lambda/lambda_function.py to handle new intent "how many attacks today" querying elastic search for attacks for current day.
- [ ] add function to alexa_warmonger_lambda/lambda_function.py to handle new intent "top countries attacking our network" replying the top 3 countries based on a query on elastic search (req on step 5 and 8) 
- [ ] Clean alexa_warmonger_lambda/ project for lambda (removing package boto3 from folder and testing it works on lambda)
