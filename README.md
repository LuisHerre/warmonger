# **Warmonger**

 Warmonger is an Alexa skill created to read snort IDS logs from a server and can answer amazon Echo requests about the status of the network.

## Files Structure

### **sensor_to_sns_parser.py**

 python code that reads the merged logs from snort , creates a SNS (Simple Notification Service) message with the data, and publish it.

### **sns_to_dynamo_lambda**
 
 Directory that contains the package to run lambda_function.py - gets SNS message from sensor_to_sns_parser.py and stores the info on dynamodb.

### **alexa_warmonger_lambda**
 
 Directory that contains the package to run lambda_function.py - answers to amazon Echo request with the information from dynamo. (based on favorite_color examples from amazon)

